module DiscreteHawkes

using Distributions, LinearAlgebra, Plots

export rand
export mvw, mvw_inf 
export estimators, fit
export plot
export DiscreteHawkesModel, DiscreteHawkesData, ConnectivityMatrix


include("model.jl")
include("simulate.jl")
include("estimation.jl")
include("plots.jl")

end
