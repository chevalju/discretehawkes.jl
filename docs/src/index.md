```@meta
CurrentModule = DiscreteHawkes
```

# DiscreteHawkes

Documentation for [DiscreteHawkes](https://gricad-gitlab.univ-grenoble-alpes.fr/chevalju/DiscreteHawkes.jl).

```@index
```

```@autodocs
Modules = [DiscreteHawkes]
```
