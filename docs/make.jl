using DiscreteHawkes
using Documenter

DocMeta.setdocmeta!(DiscreteHawkes, :DocTestSetup, :(using DiscreteHawkes); recursive=true)

makedocs(;
    modules=[DiscreteHawkes],
    authors="Julien Chevallier",
    sitename="DiscreteHawkes.jl",
    format=Documenter.HTML(;
        canonical="https://chevalju.gricad-pages.univ-grenoble-alpes.fr/DiscreteHawkes.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
    remotes=nothing,
)
