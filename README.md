# Discrete Hawkes

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://chevalju.gricad-pages.univ-grenoble-alpes.fr/DiscreteHawkes.jl/dev)
[![Build Status](https://gricad-gitlab.univ-grenoble-alpes.fr/chevalju/DiscreteHawkes.jl/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/chevalju/DiscreteHawkes.jl/pipelines)
[![Coverage](https://gricad-gitlab.univ-grenoble-alpes.fr/chevalju/DiscreteHawkes.jl/badges/main/coverage.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/chevalju/DiscreteHawkes.jl/commits/main)
