using DiscreteHawkes
using Distributions, LinearAlgebra
using Test
using Random
import DiscreteHawkes as DH

@testset "DiscreteHawkes.jl" begin
    include("model.jl")
    include("simulate.jl")
    include("estimation.jl")
end
